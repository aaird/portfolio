from flask import Flask, render_template, send_file
from flask_bootstrap import Bootstrap

app = Flask(__name__)
bootstrap = Bootstrap(app)


@app.route('/')
def info():
    return render_template('info.html')


@app.route('/CV')
def cv():
    return render_template('CV.html')


@app.route('/CV/<file>')
def doggybag(file):
    if file == 'CV':
        try:
            return send_file('./static/CV.pdf', attachment_filename='Alex Aird - CV.pdf')
        except Exception as e:
            return str(e)
    elif file == 'Studies Report':
        try:
            return send_file('./static/Studies_Report.pdf', attachment_filename='Alex Aird - Studies_Report.pdf')
        except Exception as e:
            return str(e)


@app.route('/uni')
def uni():
    return render_template('uni.html')


@app.route('/work')
def work():
    return render_template('work.html')


if __name__ == '__main__':
    app.run()
